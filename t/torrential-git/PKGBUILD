# Maintainer: Fabio 'Lolix' Loli <fabio.loli@disroot.org> -> https://github.com/FabioLolix
# Contributor: Helder Bertoldo <helder.bertoldo@gmail.com>

pkgname=torrential-git
pkgver=1.1.0.r16.g8c6d1ab
pkgrel=1
pkgdesc="Download torrents in style with this speedy, minimalist torrent client designed for Pantheon Shell"
arch=(i686 x86_64)
url="https://github.com/davidmhewitt/torrential"
license=(GPL2)
depends=(libgranite.so libevent libnatpmp libb64 dht miniupnpc libutp)
makedepends=(git cmake vala)
provides=(torrential)
conflicts=(torrential)
source=("git+https://github.com/davidmhewitt/torrential.git"
        "torrential-transmission::git+https://github.com/davidmhewitt/transmission.git#branch=2.9x-torrential"
        "torrential-no-unity.patch::https://git.archlinux.org/svntogit/community.git/plain/trunk/no-unity.patch")
sha256sums=('SKIP'
            'SKIP'
            '8b279ca266c8b1643cf10ff0a66a66283be6595959720bcc321fdef0f3da6ea7')

pkgver() {
  cd "${pkgname%-git}"
  git describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g'
}

prepare() {
  cd "${pkgname%-git}"
  install -d build

  patch -Np1 -i ../torrential-no-unity.patch
  sed -i '/--fatal-warnings/d' CMakeLists.txt

  git submodule init
  git config 'submodule.transmission.url' "${srcdir}/torrential-transmission"
  git submodule update
}

build() {
  cd "${pkgname%-git}/build"
  cmake .. \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr
  make
}

package() {
  cd "${pkgname%-git}/build"
  make DESTDIR="${pkgdir}" install
  ln -s /usr/bin/com.github.davidmhewitt.torrential "$pkgdir/usr/bin/torrential"
}

